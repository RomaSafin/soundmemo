//
//  CustomAudioCell.swift
//  S.J.C.
//
//  Created by Admin on 27.03.16.
//  Copyright © 2016 Roman.Safin. All rights reserved.
//

import Foundation

class CustomAudioCell: UICollectionViewCell {
    
    @IBOutlet weak var audioName: UILabel!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    
}
