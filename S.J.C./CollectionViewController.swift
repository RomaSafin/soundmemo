//
//  CollectionViewController.swift
//  S.J.C.
//
//  Created by Admin on 11.03.16.
//  Copyright © 2016 Roman.Safin. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import RealmSwift

class CollectionViewController: UICollectionViewController {

    let bundle = NSBundle.mainBundle()
    var globalPlayer = AVAudioPlayer()
    var defaultSoundPlayersArray:Array<AVAudioPlayer> = []
    var customSoundPlayersArray:Array<AVAudioPlayer> = []
    var sections = [String]()
    var defaultAudioNames = [String]()
    var lists : Array<CustomSound> = []
    let sectionInsets = UIEdgeInsets(top: 10.0, left: 20.0, bottom: 10.0, right: 20.0)
    var deletingMode = Bool()
    var session = AVAudioSession()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sections.append("Default Audio Memo")
        sections.append("Custom Audio Memo")
        defaultAudioNames = ["Clapping", "Cricket", "Ba Dum Tss", "Yay", "Whip"]
        defaultSoundPlayersArray = configureDefaultSounds()
        customSoundPlayersArray = configureCustomSounds()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSessionCategorySoloAmbient)
        } catch {
            print("Error set session")
        }
        do {
            try session.setActive(true)
        } catch {
            print("Error turn session")
        }
        
        let list = uiRealm.objects(CustomSound)
        lists.removeAll()
        lists = Array(list)
        if lists.count > 0 {
            lists.removeLast()
        }
        
        self.collectionView?.reloadData()
        customSoundPlayersArray = configureCustomSounds()
        deletingMode = false
        
        dispatch_after(3, dispatch_get_main_queue()) { () -> Void in
            self.collectionView?.performBatchUpdates({ () -> Void in
                let list = uiRealm.objects(CustomSound)
                self.lists.removeAll()
                self.lists = Array(list)
                if self.lists.count > 0 {
                    let indexPath = NSIndexPath(forRow: self.lists.count - 1, inSection: 1);
                    //let indexPath2 = NSIndexPath(forRow: self.lists.count, inSection: 1);
                    self.collectionView?.insertItemsAtIndexPaths([indexPath]);
                }
                }, completion: nil);
        };

        
        
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated);
        if globalPlayer.playing {
            globalPlayer.stop()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureDefaultSounds()->(Array<AVAudioPlayer>) {
        
        let clapping = bundle.pathForResource("Clapping", ofType:"mp3")
        let cricket = bundle.pathForResource("Cricket", ofType: "mp3")
        let badum = bundle.pathForResource("Badum", ofType:"mp3")
        let yay = bundle.pathForResource("Yeah", ofType: "mp3")
        let whip = bundle.pathForResource("Whip", ofType:"mp3")
        
        let soundsArray = [clapping, cricket, badum, yay, whip]
        var playerArray:Array<AVAudioPlayer> = []
        
        for item in soundsArray {
            
            let soundURL = NSURL(fileURLWithPath:item!);
            do {
                let player = try AVAudioPlayer(contentsOfURL:soundURL)
                if player.prepareToPlay() {
                    playerArray.append(player)
                    globalPlayer = player
                    
                } else {
                    print("Error prepare to play")
                }
            } catch {
                print("No sound found by URL:\(soundURL)")
            }
            
        }
        
        return playerArray
    }
    
    func configureCustomSounds()->(Array<AVAudioPlayer>) {
        
        var playerArray:Array<AVAudioPlayer> = []
        let list = uiRealm.objects(CustomSound)
        
        for item in list {
            
            let pathDocuments = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
            let filePath = pathDocuments + "/" + item.path
            let soundURL = NSURL(fileURLWithPath:filePath);
            
            do {
                let player = try AVAudioPlayer(contentsOfURL:soundURL)
                
                if player.prepareToPlay() {
                    playerArray.append(player)
                    globalPlayer = player
                } else {
                    print("Error prepare to play")
                }
            } catch {
                print("No sound found by URL:\(soundURL)")
            }
            
        }
        
        return playerArray
    }
    
    func deleteSoundFromFolder(path: String) {
        
        let pathDocuments = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        let filePath = pathDocuments + "/" + path
        do {
            try NSFileManager.defaultManager().removeItemAtPath(filePath)
        } catch {
            print("No sound found by path:\(filePath)")
        }
        
    }

    //pragma mark - UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return sections.count
    }
    
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier:"AudioHeader", forIndexPath: indexPath) as! AudioHeader
        if indexPath.section == 0 {
            headerView.deleteButton.hidden = true
        } else {
            headerView.deleteButton.hidden = false
        }
        headerView.audioCategory.text = sections[indexPath.section]
        headerView.deleteButton.addTarget(self, action: "turnEditing:", forControlEvents: UIControlEvents.TouchUpInside)
        
        if deletingMode == true {
            let image = UIImage(named: "TrashActive")
            headerView.deleteButton.setBackgroundImage(image, forState: UIControlState.Normal)
        } else {
            let image = UIImage(named: "Trash")
            headerView.deleteButton.setBackgroundImage(image, forState: UIControlState.Normal)
        }

        return headerView
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return defaultAudioNames.count
        } else {
            let numberOfItems = lists.count + 1
            return numberOfItems
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("AudioCell", forIndexPath: indexPath) as! AudioCell
        
        cell.trashButton.addTarget(self, action: "trashAction:", forControlEvents: UIControlEvents.TouchUpInside)
        
        if indexPath.section == 0 {
            cell.audioName.text = defaultAudioNames[indexPath.row]
            cell.trashButton.hidden = true
        } else {
            if indexPath.row != lists.count {
                cell.audioName.text = lists[indexPath.row].name
                if deletingMode {
                    cell.trashButton.hidden = false
                } else {
                    cell.trashButton.hidden = true
                }
            } else {
                cell = collectionView.dequeueReusableCellWithReuseIdentifier("EmptyCell", forIndexPath: indexPath) as! AudioCell
            }
        }
        
        return cell

    }
    
    //pragma mark - UICollectionViewDelegate
    
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        collectionView.deselectItemAtIndexPath(indexPath, animated: false)
        
        if globalPlayer.playing {
            globalPlayer.stop()
        }
        
        if indexPath.section == 0 {
            globalPlayer = defaultSoundPlayersArray[indexPath.row]
            globalPlayer.currentTime = 0.0
            globalPlayer.play()
        } else {
            if indexPath.row != lists.count {
                globalPlayer = customSoundPlayersArray[indexPath.row]
                globalPlayer.currentTime = 0.0
                globalPlayer.play()
            }
        }
   
    }
    
    override func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
        
        let cell = collectionView.cellForItemAtIndexPath(indexPath)
        let color = UIColor(red: 0.835, green: 0.839, blue: 0.839, alpha: 1.0)
        cell?.backgroundColor = color
        
    }
   
    override func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
        
        let cell = collectionView.cellForItemAtIndexPath(indexPath)
        cell?.backgroundColor = UIColor.whiteColor()
        
        
    }

    
    //pragma mark - UICollectionViewDelegateFlowLayout
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSizeMake(200,40)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            return sectionInsets
    }
    
//    func initialLayoutAttributesForAppearingItemAtIndexPath(itemIndexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
//        
//        let attributes: UICollectionViewLayoutAttributes = (collectionView?.layoutAttributesForItemAtIndexPath(itemIndexPath))!;
//        
//        attributes.transform3D = CATransform3DMakeScale(0.1, 0.1, 1.0)
//        
//        
//        
//        return attributes;
//    }
    
    //pragma mark - Buttons Actions
    
    func turnEditing (sender:UIButton) {
        deletingMode = !deletingMode
        collectionView?.reloadData()
    }
    
    func trashAction (sender:UIButton) {
        
        let cell = sender.superCellCollection() as! AudioCell?
        if let recentCell = cell {
            
            let indexPath = collectionView?.indexPathForCell(recentCell)
            let customAudioDelete = lists[indexPath!.row]
            let path = customAudioDelete.path
            deleteSoundFromFolder(path)
            
            try! uiRealm.write {
                uiRealm.delete(customAudioDelete)
            }
            
            let list = uiRealm.objects(CustomSound)
            lists.removeAll()
            lists = Array(list)
            
            if let index = indexPath {
                let arrayIndexPath = [index]
                collectionView?.deleteItemsAtIndexPaths(arrayIndexPath)
            }
            collectionView?.reloadData()
            
        }
        
    }


}