//
//  RealmClient.swift
//  S.J.C.
//
//  Created by Admin on 11.03.16.
//  Copyright © 2016 Roman.Safin. All rights reserved.
//

import Foundation

class RealmClient: NSObject {

    func writeFileToSoundList(attributes:NSDictionary) {
    
        let customSound = CustomSound()
        if let name = attributes["name"] {
            customSound.name = name as! String
        }
        if let path = attributes["path"] {
            customSound.path = path as! String
        }
        if let date = attributes["modificationDate"] {
            customSound.createdAt = date as! NSDate
        }
        //customSound.emoIcon = nil;
    
        try! uiRealm.write {
            uiRealm.add(customSound)
        }
    
    }
    
}