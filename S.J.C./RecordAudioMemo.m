//
//  RecordAudioMemo.m
//  S.J.C.
//
//  Created by Roman.Safin on 1/23/16.
//  Copyright © 2016 Roman.Safin. All rights reserved.
//

#import "RecordAudioMemo.h"
#import <AVFoundation/AVFoundation.h>
#import "S_J_C_-Swift.h"


@interface RecordAudioMemo () <AVAudioRecorderDelegate, AVAudioPlayerDelegate> {
    
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    AVAudioSession *session;
    NSMutableDictionary *attributesAudioFile;
    UIAlertAction *currentCreateAction;
    CAShapeLayer *circleLayer;
    CAShapeLayer *curveLayer;
    CAShapeLayer *blueLineLayer;
    
}

@end

NSString* const nameCustomAudioFolder = @"CustomAudio";
NSString* const defaultNameCustomAudio = @"MyAudioMemo.m4a";

@implementation RecordAudioMemo

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureAudioFile];
    
    NSString *pathDocumentsFolder = [(NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)) lastObject];
    NSString *customAudioFolder = [NSString stringWithFormat:@"%@/%@", pathDocumentsFolder, nameCustomAudioFolder];
    
    NSFileManager *manager = [NSFileManager defaultManager];
    BOOL succes = [manager fileExistsAtPath:customAudioFolder];
    if (!succes) {
        NSError *error = nil;
        [manager createDirectoryAtPath:customAudioFolder withIntermediateDirectories:NO attributes:nil error:&error];
        if (error) {
            NSLog(@"Error creating custom sound folder:%@", [error localizedDescription]);
        }
    } else {
        NSLog(@"Custom audio folder exists");
    }
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.recordOrStopButton.hidden = YES;
    self.playButton.hidden = YES;
    self.saveSoundButton.hidden = YES;
    self.startButton.hidden = NO;
    circleLayer.hidden = NO;
    curveLayer.hidden = YES;
    blueLineLayer.hidden = YES;
    self.plusForStartButton.hidden = NO;
    self.pressLabel.hidden = NO;
    [self.playButton setBackgroundImage:[UIImage imageNamed:@"PlayButtonGray"] forState:UIControlStateNormal];
    [self.saveSoundButton setBackgroundImage:[UIImage imageNamed:@"SaveButtonGray"] forState:UIControlStateNormal];
    [self.recordOrStopButton setBackgroundImage:[UIImage imageNamed:@"RecordButtonColored"] forState:UIControlStateNormal];
    
    if (!circleLayer) {
        [self drawAndAnimatedCircle];
    }
    [self circleAnimating];
    [self drawCurveLayer];
    
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [curveLayer removeAllAnimations];
    [blueLineLayer removeAllAnimations];
    
    if (recorder.recording) {
        [recorder stop];
    }
    if (player.playing) {
        [player stop];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    [self.playButton setBackgroundImage:[UIImage imageNamed:@"PlayButtonColored"] forState:UIControlStateNormal];
}

# pragma mark - Actions

- (IBAction)recordOrStop:(id)sender {
    
    if (player.playing) {
        [player stop];
    }
    
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session setActive:YES error:nil];
    
    if (!recorder.recording) {
        
        [recorder record];
        [self.playButton setBackgroundImage:[UIImage imageNamed:@"PlayButtonGray"] forState:UIControlStateNormal];
        [self.saveSoundButton setBackgroundImage:[UIImage imageNamed:@"SaveButtonGray"] forState:UIControlStateNormal];
        [self.recordOrStopButton setBackgroundImage:[UIImage imageNamed:@"RecordButtonColored"] forState:UIControlStateNormal];
        
        self.playButton.enabled = NO;
        self.saveSoundButton.enabled = NO;
        
        [self linesAnimating];
        curveLayer.hidden = NO;
        blueLineLayer.hidden = NO;
        
    } else {
        
        [recorder stop];
        [self.playButton setBackgroundImage:[UIImage imageNamed:@"PlayButtonColored"] forState:UIControlStateNormal];
        [self.saveSoundButton setBackgroundImage:[UIImage imageNamed:@"SaveButtonColored"] forState:UIControlStateNormal];
        [self.recordOrStopButton setBackgroundImage:[UIImage imageNamed:@"RecordButtonReuse"] forState:UIControlStateNormal];
        
        self.saveSoundButton.enabled = YES;
        self.playButton.enabled = YES;
        curveLayer.hidden = YES;
        blueLineLayer.hidden = YES;
        
        [curveLayer removeAllAnimations];
        [blueLineLayer removeAllAnimations];
        
    }
    
}

- (IBAction)playSoundMemo:(id)sender {
    
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    [session setActive:YES error:nil];
    
    if (!recorder.recording && !player.playing) {
        [self.playButton setBackgroundImage:[UIImage imageNamed:@"RecordButtonColored"] forState:UIControlStateNormal];
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
        [player setDelegate:self];
        player.volume = 0.7;
        [player play];
        
    } else if (player.playing)  {
        [self.playButton setBackgroundImage:[UIImage imageNamed:@"PlayButtonColored"] forState:UIControlStateNormal];
        [player stop];
    }
}

- (IBAction)saveSound:(id)sender {
    
    if (player.playing){
        [player stop];
    }
    
    NSString *title = @"New Audio Memo";
    NSString *doneTitle = @"Create";
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:@"Write the name of your audio memo" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *createAction = [UIAlertAction actionWithTitle:doneTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //check if file with such name exists
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSString *userFileName = [NSString stringWithFormat:@"%@", alertController.textFields.firstObject.text];
        NSString *newFilePath = [NSString stringWithFormat:@"%@/%@/%@.m4a", documentsPath, nameCustomAudioFolder, userFileName];
        if ([[NSFileManager defaultManager] fileExistsAtPath:newFilePath]) {
            UIAlertController *fileExistsAlert = [UIAlertController alertControllerWithTitle:@"Audio File Exists" message:@"Please enter other name" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self saveSound:sender];
            }];
            [fileExistsAlert addAction:okAction];
            [self presentViewController:fileExistsAlert animated:YES completion:nil];
            return;
            
        }
        
        attributesAudioFile = [NSMutableDictionary new];
        NSString *fileName = [NSString stringWithFormat:@"%@", alertController.textFields.firstObject.text];
        [attributesAudioFile setValue:fileName forKey:@"name"];
        [self renameAudioFileAndSetAttributes];
        [self configureAudioFile];
        [self.tabBarController setSelectedIndex:1];
        
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:createAction];
    [alertController addAction:cancelAction];
    createAction.enabled = false;
    currentCreateAction = createAction;
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Audio Memo Name";
        [textField addTarget:self action:@selector(audioFileNameDidChange:) forControlEvents:UIControlEventEditingChanged];
    }];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)startNewRecord:(UIButton *)sender {
    
    self.recordOrStopButton.hidden = NO;
    self.playButton.hidden = NO;
    self.saveSoundButton.hidden = NO;
    self.startButton.hidden = YES;
    circleLayer.hidden = YES;
    [circleLayer removeAllAnimations];
    curveLayer.hidden = NO;
    blueLineLayer.hidden = NO;
    self.saveSoundButton.enabled = NO;
    self.plusForStartButton.hidden = YES;
    self.pressLabel.hidden = YES;
    
    [self linesAnimating];
    
    if (player.playing) {
        [player stop];
    }
    
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session setActive:YES error:nil];
    [recorder record];
    
    [self.recordOrStopButton setTitle:@"Stop" forState:UIControlStateNormal];
    [self.playButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    self.playButton.enabled = NO;
    
}

#pragma mark - Private Methods

- (void) configureAudioFile {
    
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               defaultNameCustomAudio,
                               nil];
    
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    NSError *error = nil;
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
    
}

- (void) audioFileNameDidChange:(UITextField*) textField {
    currentCreateAction.enabled = YES;
}

- (void) renameAudioFileAndSetAttributes {
    
    NSString* fileName = [attributesAudioFile valueForKey:@"name"];
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString* newFilePath = [NSString stringWithFormat:@"%@/%@/%@.m4a", documentsPath, nameCustomAudioFolder, fileName];
    NSString* oldFilePath = [documentsPath stringByAppendingPathComponent:defaultNameCustomAudio];
    
    //rename
    NSError *error = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:oldFilePath]) {
        [[NSFileManager defaultManager] moveItemAtPath:oldFilePath toPath:newFilePath error:&error];
        if (error) {
            NSLog(@"can't rename audio: %@", [error localizedDescription]);
        }
    } else {
        NSLog(@"audio file wasn't created");
    }
    
    //add last modification date and path
    NSDictionary* attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:newFilePath error:&error];
    if (error) {
        NSLog(@"can't take attributes of file: %@", [error localizedDescription]);
    }
    NSDate *modificationDate = [attributes fileModificationDate];
    [attributesAudioFile setValue:modificationDate forKey:@"modificationDate"];
    NSString* filePathInDocuments = [NSString stringWithFormat:@"%@/%@.m4a", nameCustomAudioFolder, fileName];
    [attributesAudioFile setValue:filePathInDocuments forKey:@"path"];
    
    //write custom audio to database
    RealmClient *realmClient = [RealmClient new];
    if (attributesAudioFile.count > 0) {
        [realmClient writeFileToSoundList:attributesAudioFile];
    }
    
    //debug
    //    NSArray *componentsAtPath = [fileManager contentsOfDirectoryAtPath:documentsPath error:&error];
    //    NSLog(@"%@", componentsAtPath);
    //    NSLog(@"%@", attributesAudioFile);
    
    
}

- (void) drawAndAnimatedCircle {
    
    circleLayer = [CAShapeLayer layer];
    [[self.view layer] addSublayer:circleLayer];
    UIColor *customGreenColor = [UIColor colorWithRed:0.71 green:0.851 blue:0.349 alpha:1.0];
    [circleLayer setStrokeColor:[customGreenColor CGColor]];
    [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
    if (self.view.frame.size.height < 736) {
        [circleLayer setLineWidth:6.0];
    } else {
        [circleLayer setLineWidth:8.0];
    }
    int radius = 90;
    CGPoint drawPoint = CGPointMake(CGRectGetMidX(self.startButton.frame), CGRectGetMidY(self.startButton.frame));
    circleLayer.path = [UIBezierPath bezierPathWithArcCenter:CGPointZero radius:radius startAngle:0 endAngle:M_PI*2 clockwise:NO].CGPath;
    circleLayer.position = drawPoint;
    
}
 
- (void) circleAnimating {
    
    CABasicAnimation *buttonAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    [buttonAnimation setFromValue:[NSNumber numberWithFloat:1.0f]];
    [buttonAnimation setToValue:[NSNumber numberWithFloat:0.8f]];
    [buttonAnimation setDuration:1.2f];
    [buttonAnimation setRemovedOnCompletion:NO];
    [buttonAnimation setFillMode:kCAFillModeForwards];
    [buttonAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    buttonAnimation.repeatCount = INT_MAX;
    buttonAnimation.autoreverses = YES;
    [circleLayer addAnimation:buttonAnimation forKey:@"scale"];
    
}


- (void) drawCurveLayer {
    
    //calculate points
    CGFloat wS = self.view.frame.size.width / 14;
    CGFloat hS = self.view.frame.size.height / 24;
    CGPoint one = CGPointMake(wS * 1, hS * 8.0);
    CGPoint two = CGPointMake(wS * 1.5, hS * 6.5);
    CGPoint three = CGPointMake(wS * 3, hS * 9.5);
    CGPoint four = CGPointMake(wS * 5, hS * 5.5);
    CGPoint five = CGPointMake(wS * 7, hS * 10.5);
    CGPoint six = CGPointMake(wS * 9, hS * 4.5);
    CGPoint seven = CGPointMake(wS * 11, hS * 11.5);
    CGPoint eight = CGPointMake(wS * 13, hS * 8.0);
    
    CGPoint beginBlueLine = CGPointMake(wS * 0.95, hS * 8.0);
    CGPoint endBlueLine = CGPointMake(wS * 13, hS * 8.0);
    
    //draw curve
    UIBezierPath *linePath=[UIBezierPath bezierPath];
    
    [linePath moveToPoint:one];
    [linePath addCurveToPoint:two controlPoint1:CGPointMake(wS * 1.0, hS * 7.0) controlPoint2:CGPointMake(wS * 1.2, hS * 6.6)];
    [linePath addCurveToPoint:three controlPoint1:CGPointMake(wS * 2.8, hS * 6.5) controlPoint2:CGPointMake(wS * 1.6, hS * 9.5)];
    [linePath addCurveToPoint:four controlPoint1:CGPointMake(wS * 4.4, hS * 9.5) controlPoint2:CGPointMake(wS * 3.5, hS * 5.5)];
    [linePath addCurveToPoint:five controlPoint1:CGPointMake(wS * 6.5, hS * 5.5) controlPoint2:CGPointMake(wS * 5.3, hS * 10.5)];
    [linePath addCurveToPoint:six controlPoint1:CGPointMake(wS * 8.7, hS * 10.5) controlPoint2:CGPointMake(wS * 7.2, hS * 4.5)];
    [linePath addCurveToPoint:seven controlPoint1:CGPointMake(wS * 10.8, hS * 4.5) controlPoint2:CGPointMake(wS * 9.3, hS * 11.5)];
    [linePath addCurveToPoint:eight controlPoint1:CGPointMake(wS * 12.7, hS * 11.5) controlPoint2:CGPointMake(wS * 11.5, hS * 8.0)];

    
    curveLayer = [[CAShapeLayer alloc] init];
    [curveLayer setPath: [linePath CGPath]];
    UIColor *customGreenColor = [UIColor colorWithRed:0.71 green:0.851 blue:0.349 alpha:1.0];
    [curveLayer setStrokeColor:[customGreenColor CGColor]];
    [curveLayer setFillColor:[[UIColor clearColor] CGColor]];
    [curveLayer setLineWidth:3.0];
    [curveLayer setLineCap:@"round"];
    
    UIBezierPath *secondLinePath=[UIBezierPath bezierPath];
    [secondLinePath moveToPoint:beginBlueLine];
    [secondLinePath addLineToPoint:endBlueLine];
    
    blueLineLayer = [[CAShapeLayer alloc] init];
    [blueLineLayer setPath: [secondLinePath CGPath]];
    UIColor *customBlueColor = [UIColor colorWithRed:0.322 green:0.678 blue:0.855 alpha:1.0];
    [blueLineLayer setStrokeColor:[customBlueColor CGColor]];
    [blueLineLayer setFillColor:[[UIColor clearColor] CGColor]];
    [blueLineLayer setLineWidth:2.5];
    [blueLineLayer setLineCap:@"round"];
    
    [[self.view layer] addSublayer:curveLayer];
    [[self.view layer] addSublayer:blueLineLayer];
    curveLayer.hidden = YES;
    blueLineLayer.hidden = YES;
    
}

- (void) linesAnimating {
    
    CABasicAnimation *drawLineAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    drawLineAnimation.duration = 5.0;
    drawLineAnimation.beginTime = 0.0;
    drawLineAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    drawLineAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    drawLineAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    CABasicAnimation *dissapearLineAnimation = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
    dissapearLineAnimation.duration = 5.0;
    dissapearLineAnimation.beginTime = 5.0;
    dissapearLineAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    dissapearLineAnimation.toValue   = [NSNumber numberWithFloat:1.0f];
    dissapearLineAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.duration = 10.0;
    group.repeatCount = INT_MAX;
    group.animations = @[drawLineAnimation, dissapearLineAnimation];
    
    [curveLayer addAnimation:group forKey:@"allMyAnimations"];
    [blueLineLayer addAnimation:group forKey:@"allMyAnimations"];
    
}




@end
