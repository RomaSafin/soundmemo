//
//  RecordAudioMemo.h
//  S.J.C.
//
//  Created by Roman.Safin on 1/23/16.
//  Copyright © 2016 Roman.Safin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordAudioMemo : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *recordOrStopButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *saveSoundButton;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIImageView *plusForStartButton;
@property (weak, nonatomic) IBOutlet UILabel *pressLabel;


- (IBAction)recordOrStop:(id)sender;
- (IBAction)playSoundMemo:(id)sender;
- (IBAction)saveSound:(id)sender;
- (IBAction)startNewRecord:(UIButton *)sender;




@end
