//
//  CustomSound.swift
//  S.J.C.
//
//  Created by Admin on 11.03.16.
//  Copyright © 2016 Roman.Safin. All rights reserved.
//

import Foundation
import RealmSwift
import UIKit

class CustomSound: Object {
    
    dynamic var name = ""
    dynamic var createdAt = NSDate()
    dynamic var path = ""
    //dynamic var emoIcon = UIImage?()
    
}
