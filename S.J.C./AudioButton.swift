//
//  AudioButton.swift
//  S.J.C.
//
//  Created by Admin on 12.03.16.
//  Copyright © 2016 Roman.Safin. All rights reserved.
//

import Foundation

class AudioButton: UIButton {
    
    @IBInspectable var borderRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.clearColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
            
        }
    }
    
}
