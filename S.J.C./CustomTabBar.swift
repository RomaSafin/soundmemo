//
//  CustomTabBar.swift
//  S.J.C.
//
//  Created by Roman.Safin on 1/23/16.
//  Copyright © 2016 Roman.Safin. All rights reserved.
//

import UIKit

class CustomTabBar: UITabBarController {

    @IBInspectable var defaulSelectedTab:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedIndex = defaulSelectedTab
        
        let itemsArray = tabBar.items
        if itemsArray?.count > 0 {
            let imageNewRecord : UIImage? = UIImage(named:"RecordIconColor")!.imageWithRenderingMode(.AlwaysOriginal)
            itemsArray![0].selectedImage = imageNewRecord
            let imageCollection : UIImage? = UIImage(named:"MyCollectionColor")!.imageWithRenderingMode(.AlwaysOriginal)
            itemsArray![1].selectedImage = imageCollection
        }
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        
    }
    

    
}
