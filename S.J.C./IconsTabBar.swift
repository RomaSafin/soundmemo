//
//  IconsTabBar.swift
//  S.J.C.
//
//  Created by Admin on 25.03.16.
//  Copyright © 2016 Roman.Safin. All rights reserved.
//

import Foundation

import Foundation

class IconsTabBar: UITabBar {
    
    @IBInspectable var tintColorImage:UIColor = UIColor.clearColor() {
        didSet {
            tintColor = tintColorImage
        }
    }
}
