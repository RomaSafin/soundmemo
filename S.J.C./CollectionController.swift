//
//  CollectionController.swift
//  S.J.C.
//
//  Created by Admin on 27.03.16.
//  Copyright © 2016 Roman.Safin. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import RealmSwift

class CollectionController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, AVAudioPlayerDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let bundle = NSBundle.mainBundle()
    var globalPlayer: AVAudioPlayer!
    var defaultSoundPlayersArray:Array<AVAudioPlayer> = []
    var customSoundPlayersArray:Array<AVAudioPlayer> = []
    var sections = [String]()
    var memosImages = [UIImage]()
    var lists : Array<CustomSound> = []
    var deletingMode = Bool()
    var session = AVAudioSession()
    var currentIndexPathPlaying = NSIndexPath()
    var reusableView = AudioHeader()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sections.append("Emotion memos")
        sections.append("Your memos")
        
        memosImages.append(UIImage(named: "Clapping")!)
        memosImages.append(UIImage(named: "BaDumTss")!)
        memosImages.append(UIImage(named: "Cricket")!)
        memosImages.append(UIImage(named: "Whip")!)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        defaultSoundPlayersArray = configureDefaultSounds()
        customSoundPlayersArray = configureCustomSounds()

        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSessionCategorySoloAmbient)
        } catch {
            print("Error set session")
        }
        do {
            try session.overrideOutputAudioPort(AVAudioSessionPortOverride.Speaker)
        } catch {
            print("error in overrideOutputAudioPort")
        }
        do {
            try session.setActive(true)
        } catch {
            print("Error set session active")
        }
        
        let list = uiRealm.objects(CustomSound).sorted("createdAt")
        lists.removeAll()
        lists = Array(list)
        
        self.collectionView?.reloadData()
        customSoundPlayersArray = configureCustomSounds()
        deletingMode = false
        
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated);
        if globalPlayer.playing {
            globalPlayer.stop()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Private Methods
    
    func configureDefaultSounds()->(Array<AVAudioPlayer>) {
        
        let clapping = bundle.pathForResource("Clapping", ofType:"mp3")
        let badum = bundle.pathForResource("Badum", ofType:"mp3")
        let cricket = bundle.pathForResource("Cricket", ofType: "mp3")
        let whip = bundle.pathForResource("Whip", ofType:"mp3")
        let soundsArray = [clapping, badum, cricket, whip]
        
        var playerArray:Array<AVAudioPlayer> = []
        
        for item in soundsArray {
            
            let soundURL = NSURL(fileURLWithPath:item!);
            do {
                let player = try AVAudioPlayer(contentsOfURL:soundURL)
                //player.delegate = self
                if player.prepareToPlay() {
                    playerArray.append(player)
                    globalPlayer = player
                    
                } else {
                    print("Error prepare to play")
                }
            } catch {
                print("No sound found by URL:\(soundURL)")
            }
            
        }
        
        return playerArray
    }
    
    func configureCustomSounds()->(Array<AVAudioPlayer>) {
        
        var playerArray:Array<AVAudioPlayer> = []
        let list = uiRealm.objects(CustomSound).sorted("createdAt")
        
        for item in list {
            
            let pathDocuments = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
            let filePath = pathDocuments + "/" + item.path
            let soundURL = NSURL(fileURLWithPath:filePath);
            
            do {
                let player = try AVAudioPlayer(contentsOfURL:soundURL)
                player.delegate = self
                
                if player.prepareToPlay() {
                    playerArray.append(player)
                    globalPlayer = player
                } else {
                    print("Error prepare to play")
                }
            } catch {
                print("No sound found by URL:\(soundURL)")
            }
            
        }
        
        return playerArray
    }
    
    func deleteSoundFromFolder(path: String) {
        
        let pathDocuments = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        let filePath = pathDocuments + "/" + path
        do {
            try NSFileManager.defaultManager().removeItemAtPath(filePath)
        } catch {
            print("No sound found by path:\(filePath)")
        }
        
    }
    
    //MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return sections.count
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier:"AudioHeader", forIndexPath: indexPath) as! AudioHeader
        
        if indexPath.section == 0 {
            headerView.deleteButton.hidden = true
        } else if indexPath.section == 1 && lists.count == 0 {
            headerView.deleteButton.hidden = true
        } else {
            headerView.deleteButton.hidden = false
            reusableView = headerView
        }
        
        if indexPath.section == 1 && lists.count == 0 {
            headerView.audioCategory.text = ""
        } else {
            headerView.audioCategory.text = sections[indexPath.section]
        }
        headerView.deleteButton.addTarget(self, action: "turnEditing:", forControlEvents: UIControlEvents.TouchUpInside)
        
        return headerView
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0 {
            return memosImages.count
        } else {
            return lists.count
            
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let defaultCell = collectionView.dequeueReusableCellWithReuseIdentifier("DefaultAudioCell", forIndexPath: indexPath) as! DefaultAudioCell
        
        let customCell = collectionView.dequeueReusableCellWithReuseIdentifier("CustomAudioCell", forIndexPath: indexPath) as! CustomAudioCell
        
        customCell.deleteButton.addTarget(self, action: "trashAction:", forControlEvents: UIControlEvents.TouchUpInside)
        
        if indexPath.section == 0 {
            defaultCell.imageCell.image = memosImages[indexPath.row]
            return defaultCell
        } else {
            customCell.audioName.text = lists[indexPath.row].name
            let color = UIColor(red: 0.667, green: 0.804, blue: 0.294, alpha: 1.0)
            customCell.borderView.layer.borderColor = color.CGColor
            if (self.view.frame.size.height < 736) {
                customCell.borderView.layer.borderWidth = 2.0
            } else {
                customCell.borderView.layer.borderWidth = 3.0
            }
            if deletingMode {
                customCell.deleteButton.hidden = false
            } else {
                customCell.deleteButton.hidden = true
            }
            return customCell
        }
        
    }
    
    //MARK: UICollectionViewDelegate
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        if globalPlayer != nil && globalPlayer.playing {
            globalPlayer.stop()
            if currentIndexPathPlaying.section == 1 {
                let cell = collectionView.cellForItemAtIndexPath(currentIndexPathPlaying) as? CustomAudioCell
                if cell != nil {
                    cell!.audioName.textColor = UIColor(red: 0.325, green: 0.322, blue: 0.02, alpha: 1.0)
                }
            }
        }
        
        if indexPath.section == 0 {
            globalPlayer = defaultSoundPlayersArray[indexPath.row]
            globalPlayer.currentTime = 0.0
            globalPlayer.play()
        } else {
            globalPlayer = customSoundPlayersArray[indexPath.row]
            globalPlayer.currentTime = 0.0
            globalPlayer.delegate = self
            globalPlayer.play()
            currentIndexPathPlaying = indexPath
            
            let cell = collectionView.cellForItemAtIndexPath(indexPath) as! CustomAudioCell
            cell.audioName.textColor = UIColor(red: 0.322, green: 0.678, blue: 0.855, alpha: 1.0)
        }
        
            
    }
    
    func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
        
        //let cell = collectionView.cellForItemAtIndexPath(indexPath)
        //let color = UIColor(red: 0.835, green: 0.839, blue: 0.839, alpha: 1.0)
        //cell?.backgroundColor = color
        
    }
    
    func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
        
        //let cell = collectionView.cellForItemAtIndexPath(indexPath)
        //cell?.backgroundColor = UIColor.whiteColor()
        
        
    }
    
    
    //MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        if indexPath.section == 0 {
            let width = (self.view.bounds.size.width - (self.view.bounds.size.width * 0.219)) / 4.0
            let height = self.view.bounds.size.height * 0.09
            return CGSizeMake(width, height)
        } else {
            let width = (self.view.bounds.size.width - (self.view.bounds.size.width * 0.16)) / 2.0
            let height = self.view.bounds.size.height * 0.09
            return CGSizeMake(width, height)
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        let sectionInsets = UIEdgeInsets(top: 10.0, left: 20.0, bottom: 10.0, right: 20.0)
        return sectionInsets
    }
    
    //MARK: Buttons Actions
    
    func turnEditing (sender:UIButton) {
        deletingMode = !deletingMode
        collectionView?.reloadData()
    }
    
    func trashAction (sender:UIButton) {
        
        let cell = sender.superCellCollection() as! CustomAudioCell?
        
        if let recentCell = cell {
            
            let indexPath = collectionView?.indexPathForCell(recentCell)
            
            if let index = indexPath {
                
                let customAudioDelete = lists[index.row]
                let path = customAudioDelete.path
                deleteSoundFromFolder(path)
                
                try! uiRealm.write {
                    uiRealm.delete(customAudioDelete)
                }
                
                let list = uiRealm.objects(CustomSound).sorted("createdAt")
                lists.removeAll()
                lists = Array(list)
                customSoundPlayersArray = configureCustomSounds()

                let arrayIndexPath = [index]
                collectionView?.deleteItemsAtIndexPaths(arrayIndexPath)
                
                if lists.count == 0 {
                    reusableView.audioCategory.text = ""
                    reusableView.deleteButton.hidden = true
                }
                collectionView?.reloadData()
                
            }
            
        }
        
    }

    // MARK: AVAudioPlayerDelegate

    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
    
        //print("audio did finish playing, result:\(flag)")
        let cell = collectionView.cellForItemAtIndexPath(currentIndexPathPlaying) as! CustomAudioCell
        cell.audioName.textColor = UIColor(red: 0.325, green: 0.322, blue: 0.02, alpha: 1.0)

    
    }

    func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer, error: NSError?) {
    
        print("error stop playing: \(error?.localizedDescription)")
    
    }
}