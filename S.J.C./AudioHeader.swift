//
//  AudioHeader.swift
//  S.J.C.
//
//  Created by Admin on 11.03.16.
//  Copyright © 2016 Roman.Safin. All rights reserved.
//

import Foundation

class AudioHeader: UICollectionReusableView {
    
    @IBOutlet weak var audioCategory: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
}