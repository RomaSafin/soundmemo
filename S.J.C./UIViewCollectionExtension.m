//
//  UIViewCollectionExtension.m
//  S.J.C.
//
//  Created by Admin on 14.03.16.
//  Copyright © 2016 Roman.Safin. All rights reserved.
//

#import "UIViewCollectionExtension.h"

#import "UIViewCollectionExtension.h"

@implementation UIView (UIViewCollectionExtension)

- (UICollectionViewCell*) superCellCollection {
    
    if (!self.superview) {
        return nil;
    }
    
    if ([self.superview isKindOfClass:[UICollectionViewCell class]]) {
        return (UICollectionViewCell*) self.superview;
    }
    
    return [self.superview superCellCollection];
}

@end
