//
//  UIViewCollectionExtension.h
//  S.J.C.
//
//  Created by Admin on 14.03.16.
//  Copyright © 2016 Roman.Safin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UIViewCollectionExtension)

- (UICollectionViewCell*) superCellCollection;

@end
