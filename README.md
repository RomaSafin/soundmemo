# SoundMemo #
Soundmemo is ideal tool, that helps you in situations, when there are no words to comment stupid and unfunny jokes. Also it's perfect to storage your own audio memos, such as unusual laugh of your friend, interesting sounds and thoughts, favourite comments and so on. It has cool user interface, animations and is easy to use, that make it very simple and intuitive.

## In this application you can: ##
* use some default sound memos: clapping, sound of drums and etc., which you can use as sound accompaniment for stupid or funny situations 
* application has voice recorder, so you can record all you need with no time limit on recording by a single tap on your device 
* instantly play back, specifically listen your audio before saving 
* delete your recording files 
* use and play your records at any time, because they are stored on your device automatically organized by date

## In this app I used: ##
1. Swift in combination with Objective-c.  
2. Cocoapods for downloading additional libraries. 
3. RealmSwift as database for creating data models, storing objects for audio files and access them. 
4. CollectionView for showing default and custom audio files, it's delegate for selecting sounds and playing them when selected. Collection has editing mode, when user wants to delete unnessesary audio. 
5. NSFileManager for creating folders, managing audio files, saving them in documents folder and deleting. 
6. AVFoundation, exspecially AVAudioPlayer and AVAudioRecorder for playing and recording audio, and AVAudioSession to set sounds playing, it's category and port. AVAudioPlayer delegate was used to observe when audio stop playing for reloading views and collection.
7. CAShapeLayers for drawing elements like circles and lines, UIBezierPath for drawing curve line and to add them to controllers.
8. CABasicAnimation and it's CAAnimationGroup to animate every CAShapeLayers, creating sound vibrations emulator when recording.
9. UIAlertController and it's custom setting (own text field) which helps user to set name for his audio and save it, also prevent him from setting name which already exists. 
10. TabBarController for easy surfing between two screens by one tap. 
11. Custom audio cells and headers in collection for easy grouping and using sound memos.
12. Extension for UIView, which hepls to access to view by subview, using recursion.
13. Assets file for storing custom images.  
14. Build screen using size classes and autolayout for multiple screen sizes.

### Link to app in AppStore: ###
[Soundmemo](https://itunes.apple.com/ua/app/soundmemo/id969851036?mt=8)

### Screenshots: ###
![Simulator Screen Shot 30 марта 2016 г., 3.29.38.png](https://bitbucket.org/repo/y45L4j/images/3288891005-Simulator%20Screen%20Shot%2030%20%D0%BC%D0%B0%D1%80%D1%82%D0%B0%202016%20%D0%B3.,%203.29.38.png) ![Simulator Screen Shot 30 марта 2016 г., 3.21.47.png](https://bitbucket.org/repo/y45L4j/images/3213036726-Simulator%20Screen%20Shot%2030%20%D0%BC%D0%B0%D1%80%D1%82%D0%B0%202016%20%D0%B3.,%203.21.47.png) ![Simulator Screen Shot 30 марта 2016 г., 3.22.34.png](https://bitbucket.org/repo/y45L4j/images/4111872356-Simulator%20Screen%20Shot%2030%20%D0%BC%D0%B0%D1%80%D1%82%D0%B0%202016%20%D0%B3.,%203.22.34.png)